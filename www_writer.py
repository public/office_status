import time
import requests
import json
import logging
import sys

html_file = '/users/n3parikh/www/office-status.html'
json_file = '/users/n3parikh/www/office-status.json'
csv_file = '/users/n3parikh/www/office-status.csv'

def writeOpenHTML(lux):
    hmtl = \
"""<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CSC Office Open</title>
The office is currently open.<br>
Updated At: %s<br>
Lux: %s""" % (time.strftime('%l:%M%p %Z on %b %d, %Y'), lux)
    with open(html_file, 'w') as f:
        f.write(hmtl)

def writeClosedHTML(lux):
    hmtl = \
"""<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>CSC Office Closed</title>
The office is currently closed.<br>
Updated At: %s<br>
Lux: %s""" % (time.strftime('%l:%M%p %Z on %b %d, %Y'), lux)
    with open(html_file, 'w') as f:
        f.write(hmtl)

def writeUnknownHTML():
    hmtl = \
"""<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Status Unknown</title>
Office status unknown.<br>
Updated At: %s<br>""" % (time.strftime('%l:%M%p %Z on %b %d, %Y'))
    with open(html_file, 'w') as f:
        f.write(hmtl)

def writeJSON(status):
    out_json = {"status": status, "time": time.time()}
    with open(json_file, 'w') as f:
        f.write(json.dumps(out_json))

def writeCSV(status):
    out = "%f,%d\n" % (time.time(), status)
    with open(csv_file, 'a') as f:
        f.write(out)

logging.basicConfig(filename='/users/n3parikh/office/log',
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.ERROR)

logger = logging.getLogger('office')

try:
    r = requests.get('http://powernap:5000', timeout=1)
except Exception as e:
    logger.error(e)
    writeUnknownHTML()
    writeJSON(-1)
    writeCSV(-1)
    sys.exit()

if r.status_code == 200 and r.text != "error":
    l = float(r.text)
    if l > 10:
        status = 1
    else:
        status = 0
else:
    status = -1

if status == 1:
    writeOpenHTML(r.text)
elif status == 0:
    writeClosedHTML(r.text)
else:
    logger.error(r.text)
    writeUnknownHTML()
writeJSON(status)
writeCSV(status)
