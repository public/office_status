import serial
import time
from flask import Flask

app = Flask(__name__)

@app.route("/")
def lux():
    ser = serial.Serial('/dev/ttyACM0')
    # Remove trailing newline (for some reason strip() doesn't work)
    data = str(ser.readline()).split("\\")[0]
    if data:
        l = float(data.split("=")[1])
        return str(l)
    else:
        return "error"

app.run(host="0.0.0.0")
