import time
import board
import busio

from adafruit_apds9960 import colorutility
from adafruit_apds9960.apds9960 import APDS9960

i2c = busio.I2C(board.SCL,board.SDA)
sensor = APDS9960(i2c)
sensor.enable_color = True

while True:
    while not sensor.color_data_ready:
        time.sleep(0.005)
    r,g,b,c = sensor.color_data
    print(f"lux={colorutility.calculate_lux(r,g,b)}")
    time.sleep(1)

